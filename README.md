# PostgreSQL

### **Por que escolher?**  
1.	Possui todos os requisitos de um banco de dados transacional
2.	Totalmente compatível com propriedades ACID (Atomicidade, Consistência, Isolamento e Durabilidade)
3.	Suporta views, procedimentos armazenados, triggers e outros tipos de objetos comuns a bancos de dados relacionais
4.	Permite operações geoespaciais com o Postgis.


### Comandos utilitários psql ( utilizando command line interface)

Segue uma lista de comandos para a administração dos recursos de permissões de usuário do database postgres:
- **Nota: para fazer uso do comando psql é necessário que o caminho do diretório esteja indicado nas variáveis de ambiente do sistema operacional.**
1.	psql –version 	exibe a versão instalada do postgres.
2.	psql –help		exibe uma lista de comandos internos do postgres

Operações internas do bd também podem ser realizadas pelo CLI, e para isso, basta dar o comando psql -U “usuáro do banco” 
 Por padrão, após a instalação do postgres, é criado um super usuário (tem todas as permissões) chamado postgres, no entanto manter este usuário padrão o torna suscetível à uma vulnerabilidade na segurança do seu banco de dados.
Executando o comando ** psql -U postgres **, o cli pedirá a senha que você determinou durante a instalação no seu S.O.
Quando logado, você perceberá que após o nome do usuário terá “=#”, que indica que você está com as permissões de superusuário.
Caso queira alterar a senha de usuário nesse estado, basta dar o comando ** \password ** , e então digitar sua nova senha.
Caso queira consultar os comandos SQL, basta usar o comando ** \h ** e caso queira informações detalhados sobre um comando específico, ** \h  “comando” **, sendo “comando” a operação de seu interesse.
Em caso de consulta dos comandos da interface em si, digite ** \? ** 


### listando os comandos

    1.	\l : lista os bancos encontrados pelo sistema.
    2.	\du : lista os usuários encontrados.
    3.	\c : muda para um banco de dados específico.
    4.	\d : mostra as tabelas presentes no banco de dados atual
    5.	\! : alterna para o modo console do S.O. (pode voltar para o CLI do psql com o comando “exit”)
    6.	\q : encerra a operação atual do utilitário psql.

** NOTA : é importante reforçar que o arquivo pg_hba.conf esteja configurado para pedir a senha de usuário para aumentar um pouco a integridade da segurança do banco de dados. Você poderá consultar esse arquivo enquanto estiver logado pelo utilitário do psql através do comando “show hba_file;”, que retornará o endereço local do arquivo em questão. **
### Comandos do banco de dados (Utilizando GUI)
 - Criando um banco de dados:  <br>
        CREATE DATABASE banco_exemplo <br>
        WITH OWNER = proprietário <br>
        ENCODING = “UTF8” <br>
        LC_COLLATE = ‘pt BR.UTF-8’ <br>
        LC_CTYPE = ‘pt BR.UTF-8’ <br>
        TABLESPACE = pg default <br>
        CONNECTION LIMIT  -1; <br>

<br>

**Nota**:(connection limit -1 apenas caso queira deixar ilimitado o número de conexões simultâneas, não recomendado em determinados tipos de procedimentos.)<br>
**Nota**: as configurações citadas acima são consideradas opções padrão de criação, entretanto foram listadas as opções em caso de necessidade de altereação.<br>
Caso haja a necessidade de fornecer permissões especiais para um usuário do banco de dados, logado no CLI do psql como superusuário, utiliza-se o comando:<br>
**GRANT “permissão” ON DATABASE “banco_exemplo” TO “usuário em questão”**

 - Excluindo um banco de dados:
1. DROP DATABASE banco_exemplo

